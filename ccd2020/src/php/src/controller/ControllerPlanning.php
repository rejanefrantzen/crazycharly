<?php
namespace charlyday\controller;
use charlyday\modele\Cycle;
use charlyday\vue\Vue;

class ControllerPlanning {

    public function getCycle($num){
        $cycle=Cycle::where("num", "=", $num)->first();
        $vue=new Vue($cycle,Vue::CYCLE);
        $vue->render();
    }
}