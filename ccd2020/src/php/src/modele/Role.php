<?php
namespace charlyday\modele;

class Role extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'role';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function __toString(){
        return 'id: '.$this->id.', label: '.$this->label;
    }
}