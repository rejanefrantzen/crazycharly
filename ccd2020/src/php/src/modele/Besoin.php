<?php
namespace charlyday\modele;

class Besoin extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'besoin';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function __toString(){
        $res="<div class='besoin'>";
        $res.="<p>nombre de personne: $this->nb</p>";
        $res.="</div>";
        return $res;
    }
}