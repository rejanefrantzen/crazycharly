<?php
namespace charlyday\modele;

class Creneau extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'Creneau';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function __toString(){
        $res="<div class='creneau'><p>$this->debut heure à $this->fin heure</p>";
        $besoin=Besoin::where("creneau", "=", $this->id)->get();
        foreach($besoin as $i)
            $res.=$i;
        $res.="</div>";
        return $res;
    }
}