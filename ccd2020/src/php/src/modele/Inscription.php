<?php
namespace charlyday\modele;

class Inscription extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'inscription';
    protected $primaryKey = ['user', 'besoin'];
    public $timestamps = false;
}