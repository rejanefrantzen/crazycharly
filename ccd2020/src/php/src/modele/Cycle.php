<?php
namespace charlyday\modele;

class Cycle extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'cycle';
    protected $primaryKey = 'num';
    public $timestamps = false;

    public function __toString(){
        $res="<div class='cycle'><p>cycle numero: $this->num</p>";
        foreach(['A', 'B', 'C', 'D'] as $s){
            $res.="<section class='secsemaine'><div class='semaine' id='$s'>";
            $res.="<p>Semaine $s:</p>";
            for($j=1;$j<8;$j++){
                $res.="<section class='secjour'><div class='jour' id=$j>";
                $res.="<p>Jour $j:</p>";
                $creneaujour=Creneau::where("cycle", "=", $this->num)->where("semaine", "=", $s)->where("jour", "=", $j)->get();
                foreach($creneaujour as $i)
                    $res.=$i;
                $res.="</div></section>";
            }
            $res.="</div></section>";
        }
        $res.='</div>';
        return $res;
    }
}