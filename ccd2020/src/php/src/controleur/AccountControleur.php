<?php
namespace charlyday\controleur;
use charlyday\modele\Account as Account;


class AccountControleur {

  private $errorMessage = "";

  // Vérifie si l'utilisateur est connecté
  public static function isConnected() {
    return isset($_SESSION['user_connected']) && $_SESSION['user_connected'] == true;
  }

  private static function setConnected($bool) {
    $_SESSION['user_connected'] = $bool ? true : false;
  }


  /**
   *Créer un nouveau compte
   */
  function insertNewAccount() {
    $acc = new Account();
    $vue = new AccountView();

    // Vérifie les données reçues
    if (!isset($_POST['acc_nom'])) $vue->error("entrez un nom");
    if (!isset($_POST['acc_prenom'])) $vue->error("entrez un prénom");
    if (Account::withTrashed()->where('login','=', strtolower($_POST['acc_login']))->first() != null) $vue->error("Ce login est déjà pris");
    if (!isset($_POST['acc_login']) || strlen($_POST['acc_login']) < 3) $vue->error("entrez un login suffisamment long");
    if (!isset($_POST['acc_password']) || strlen($_POST['acc_password']) < 8) $vue->error("entrez un mot de passe suffisamment long");
    if (!isset($_POST['acc_password_confirmation']) || strlen($_POST['acc_password_confirmation']) < 8) $vue->error("vous devez confirmer votre mot de passe");
    if ($_POST['acc_password'] != $_POST['acc_password_confirmation']) $vue->error("les mots de passe entrés ne concordent pas");

    $acc->nom = $_POST['acc_nom'];
    $acc->prenom = $_POST['acc_prenom'];
    $acc->login = strtolower($_POST['acc_login']);
    $acc->password = crypt($_POST['acc_password'], "sel de mer");
    $acc->admin = false;

    // Enregistre le nouveau compte
    try {
      if ($acc->save()) {
        $vue = new MainView();
        $vue->addHeadMessage('Votre compte a bien été créé !', 'good');
        $vue->render($acc);
      } else {
        $vue->addHeadMessage('Impossible de créer le compte', 'bad');
        $vue->renderAccountEditor($acc);
      }
    } catch (QueryException $e) {
      $vue->addHeadMessage("Une erreur est survenue à la sauvegarde...", "bad");
    }
  }

  /**
   * Génère le header affichant l'utilisateur connecté
   */
  static function generateAccountHeader() {
    $content = "";
    AccountView::generateAccountHeader(AccountControleur::isConnected(), AccountControleur::getCurrentUser());
  }

  /**
   *Créer un formulaire de compte
   */
  function createAccountForm() {
    $view = new AccountView();
    $view->renderAccountEditor(null);
  }

  /**
   *Récupérer le login de la personne qui est connectée
   */
  static function getLogin() {
    if (isset($_SESSION['user_login']))
      return $_SESSION['user_login'];
    else
      return "";
  }

  /**
   *Permet de se connecter
   */
  function connect() {
    $vue = new GlobalView();

    if (!isset($_POST['acc_login']))
      echo ("entrez un login");

    if (!isset($_POST['acc_password']))
      echo ("entrez votre mot de passe");

    $login = $_POST['acc_login'];
    $password = $_POST['acc_password'];

    $acc = Account::where('login', '=', strtolower($login))->first();

    if ($acc == null || crypt($password, 'sel de mer') != $acc->password )
      echo("Erreur d'identification");
	
    $_SESSION['user_connected'] = true;
    $_SESSION['user_login'] = $acc->login;
    $_SESSION['user_id'] = $acc->id_account;

    echo("Vous êtes connecté !");
/*
    if (isset($_SERVER['HTTP_REFERER'])) {
      // Retourne à la page précédente
      \Slim\Slim::getInstance()->redirect($_SERVER['HTTP_REFERER'], 303);
    }
    else {
      $vue = new GlobalView();
      $vue->render();
    }*/
  }

  /**
   * Permet de se déconnecter
   */
  function disconnect() {
    if (AccountControleur::isConnected()) {
      $_SESSION['user_connected'] = false;
      $_SESSION['user_login'] = "";
      $_SESSION['user_id'] = 0;
      GlobalView::addHeadMessage("Vous êtes à présent déconnecté(e)", "good");
    }
    $app = \Slim\Slim::getInstance();
    $app->deleteCookie('user');
    $app->deleteCookie('pass');
    $app->redirect($app->urlFor('home'), 303);
  }

  /**
  * Retourne l'utilisateur
  * @return $user l'utilisateur
  */
  static function getCurrentUser(){
    if (!isset($_SESSION['user_id'])) return null;
    $user = Account::where('id_account','=',$_SESSION['user_id'])->first();
    return $user;
  }

  /**
  * Se connecte à partir des cookies si possible
  */
  static function connectFromCookie($c_user, $c_pass){
    if (!isset($c_user) || !isset($c_pass)) return;
    $user = Account::where('login','=',$c_user)->first();
    if ($c_pass == crypt($user->login, $user->password)) {
      AccountControleur::setConnected(true);
      $_SESSION['user_login'] = $user->login;
      $_SESSION['user_id'] = $user->id_account;
    }
  }
}
