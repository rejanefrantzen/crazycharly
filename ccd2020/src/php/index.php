<?php
require_once __DIR__ . '/vendor/autoload.php';
use charlyday\controller\ControllerPlanning;
use  charlyday\controleur\AccountControleur;

use Illuminate\Database\Capsule\Manager as DB;
$db = new DB();
$db->addConnection(parse_ini_file('conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim;

// Démarre la session
session_start();


if (AccountControleur::isConnected()) {
$user = AccountControleur::getCurrentUser();
$app->setCookie('user', $user->login, '12 months');
$app->setCookie('pass', crypt($user->login, $user->password), '12 months');
}

if ($app->getCookie('user') && $app->getCookie('pass')) {
AccountControleur::connectFromCookie($app->getCookie('user'), $app->getCookie('pass'));
}

$app->get('/', function(){
    $c=new ControllerPlanning();
    $c->getCycle(1);
})->name('home');

$app->run();